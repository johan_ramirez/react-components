import React from 'react';
import styles from './TextArea.module.css';

interface TextAreaProps {
	id: string;
	name?: string;
	labelText?: string;
	placeholderText?: string;
	onChange?: React.ChangeEventHandler;
	value?: string;
	className: string;
}

export default function TextArea({
	id,
	name,
	labelText,
	placeholderText,
	onChange,
	value,
	className,
}: TextAreaProps) {
	return (
		<span className={styles.textareaWrapper + ' col ' + className}>
			<label className={styles.label} htmlFor={id}>
				{labelText}
			</label>
			<textarea
				className={styles.textarea}
				value={value}
				onChange={onChange}
				id={id}
				name={name}
				placeholder={placeholderText}
			/>
		</span>
	);
}
