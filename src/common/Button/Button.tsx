import React from 'react';
import styles from './Button.module.css';

interface ButtonProps {
	buttonText?: string;
	onClick?: React.MouseEventHandler;
}

const Button = ({ buttonText, onClick }: ButtonProps) => (
	<button className={styles.btn} onClick={onClick}>
		{buttonText}
	</button>
);

export default Button;
