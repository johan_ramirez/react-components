import React from 'react';
import Logo from './components/Logo/Logo';
import Button from 'src/common/Button/Button';
import styles from './Header.module.css';

function Header() {
	return (
		<header className={styles.header}>
			<Logo />
			<span className='spacer' />
			<span>Ryan</span>
			<Button buttonText='Logout' />
		</header>
	);
}

export default Header;
