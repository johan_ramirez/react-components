import React from 'react';
import Button from 'src/common/Button/Button';

export default function EmptyCourseList({ onClickAdd }) {
	return (
		<div className='centered-content col'>
			<h2>Course List is Empty</h2>
			<span className='mb3'>
				Please use "Add New Course" button to add your first course
			</span>
			<Button onClick={onClickAdd} buttonText='Add New Course' />
		</div>
	);
}
