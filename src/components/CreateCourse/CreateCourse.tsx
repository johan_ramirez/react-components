import React, { useState } from 'react';
import Button from 'src/common/Button/Button';
import Input from 'src/common/Input/Input';
import styles from './CreateCourse.module.css';
import TextArea from 'src/common/TextArea/TextArea';
import { Author, Course } from 'src/types';
import AuthorsAndDuration from './components/AuthorsAndDuration/AuthorsAndDuration';
import generateAuthorId from 'src/helpers/generateAuthorId';
import getCreationDate from 'src/helpers/getCreationDate';

export default function CreateCourse({
	authors,
	addAuthor,
	addCourse,
}: {
	authors: Author[];
	addAuthor: (name: string) => void;
	addCourse: (course: Course) => void;
}) {
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [inputValues, setInputValues] = useState({
		title: '',
		description: '',
		duration: '',
	});

	const handleInputChange = (event) => {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		setInputValues({ ...inputValues, [name]: value });
	};

	const addCourseAuthor = (id: string) => {
		const author = authors.find((author) => author.id == id);
		setCourseAuthors([...courseAuthors, author]);
	};

	const removeCourseAuthor = (id: string) => {
		const index = courseAuthors.findIndex((author) => author.id == id);
		const newCourseAuthors = [...courseAuthors];
		newCourseAuthors.splice(index, 1);
		setCourseAuthors(newCourseAuthors);
	};

	const filteredAuthors = authors.filter(
		(author) =>
			courseAuthors.findIndex((courseAuthor) => courseAuthor.id == author.id) ==
			-1
	);

	const handleCreateCourse = () => {
		if (
			inputValues.title.trim().length < 1 ||
			inputValues.description.trim().length < 1 ||
			inputValues.duration.trim().length < 1
		) {
			alert('Please, fill in all fields');
			return;
		}

		addCourse({
			id: generateAuthorId(),
			title: inputValues.title,
			description: inputValues.description,
			creationDate: getCreationDate(),
			duration: parseInt(inputValues.duration),
			authors: courseAuthors.map((author) => author.id),
		});
	};

	return (
		<div className={styles.createcourse}>
			<div className='row align-center'>
				<Input
					id='title'
					name='title'
					labelText='Title'
					placeholderText='Enter title'
					value={inputValues.title}
					onChange={handleInputChange}
				/>
				<span className='spacer' />
				<Button buttonText='Create course' onClick={handleCreateCourse} />
			</div>
			<TextArea
				id='description'
				name='description'
				labelText='Description'
				placeholderText='Enter description'
				className='w100'
				value={inputValues.description}
				onChange={handleInputChange}
			/>
			<AuthorsAndDuration
				addAuthor={addAuthor}
				filteredAuthors={filteredAuthors}
				courseAuthors={courseAuthors}
				removeCourseAuthor={removeCourseAuthor}
				addCourseAuthor={addCourseAuthor}
				handleInputChange={handleInputChange}
				durationValue={inputValues.duration}
			/>
		</div>
	);
}
