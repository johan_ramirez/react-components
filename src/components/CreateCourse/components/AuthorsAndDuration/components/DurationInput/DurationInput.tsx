import React from 'react';
import Input from 'src/common/Input/Input';
import styles from './DurationInput.module.css';
import getCourseDuration from 'src/helpers/getCourseDuration';

export default function DurationInput({
	value,
	onChange,
}: {
	value: string;
	onChange: React.ChangeEventHandler;
}) {
	return (
		<div className='col w48 align-center gap2'>
			<span>
				<b>Duration</b>
			</span>
			<Input
				className='w100'
				id='duration'
				name='duration'
				type='number'
				labelText='Duration'
				placeholderText='Enter duration in minutes'
				value={value}
				onChange={onChange}
			/>
			<span className={styles.duration}>
				Duration: {getCourseDuration(value ? parseInt(value) : 0)}
			</span>
		</div>
	);
}
