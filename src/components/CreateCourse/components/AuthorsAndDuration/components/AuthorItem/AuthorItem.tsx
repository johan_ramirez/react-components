import React, { MouseEventHandler } from 'react';
import Button from 'src/common/Button/Button';
import { Author } from 'src/types';

export default function AuthorItem({
	author,
	onClick,
	courseAuthor,
}: {
	author: Author;
	onClick: MouseEventHandler;
	courseAuthor?: boolean;
}) {
	const buttonText = courseAuthor ? 'Delete author' : 'Add author';

	return (
		<div className='row w100 justify-between'>
			<span>{author.name}</span>
			<Button buttonText={buttonText} onClick={onClick} />
		</div>
	);
}
