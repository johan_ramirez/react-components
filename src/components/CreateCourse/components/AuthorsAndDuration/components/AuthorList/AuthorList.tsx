import { Author } from 'src/types';
import styles from './AuthorList.module.css';
import AuthorItem from '../AuthorItem/AuthorItem';
import React from 'react';

export default function AuthorList({
	authors,
	courseAuthors,
	handleClick,
}: {
	authors: Author[];
	courseAuthors?: boolean;
	handleClick: (id: string) => void;
}) {
	return (
		<div className={styles.authorsWrapper}>
			{authors.length > 0 ? (
				authors.map((author) => (
					<AuthorItem
						author={author}
						onClick={() => handleClick(author.id)}
						courseAuthor={courseAuthors}
					/>
				))
			) : (
				<span>Author list is empty</span>
			)}
		</div>
	);
}
