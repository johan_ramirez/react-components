import React, { useState } from 'react';
import Header from './components/Header/Header';
import { mockedAuthorsList, mockedCoursesList } from './constants';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import generateAuthorId from './helpers/generateAuthorId';
import { Course } from './types';
import EmptyCourseList from './components/EmptyCourseList/EmptyCourseList';

enum PageStates {
	courses,
	newCourse,
}

function App() {
	const [pageState, setPageState] = useState(PageStates.courses);
	const [courses, setCourses] = useState([]);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const addAuthor = (name: string) => {
		if (name.trim().length < 1) {
			alert('Author name should not be empty');
			return;
		}

		setAuthors([
			...authors,
			{
				id: generateAuthorId(),
				name: name,
			},
		]);
	};

	const addCourse = (course: Course) => {
		setCourses([...courses, course]);
		setPageState(PageStates.courses);
	};

	function getPage() {
		switch (pageState) {
			case PageStates.courses:
				if (courses.length == 0) {
					return (
						<EmptyCourseList
							onClickAdd={() => setPageState(PageStates.newCourse)}
						/>
					);
				}
				return (
					<Courses
						onClickAdd={() => setPageState(PageStates.newCourse)}
						courses={courses}
						authors={authors}
					/>
				);
			case PageStates.newCourse:
				return (
					<CreateCourse
						authors={authors}
						addAuthor={addAuthor}
						addCourse={addCourse}
					/>
				);
		}
	}

	return (
		<>
			<Header />
			{getPage()}
		</>
	);
}

export default App;
