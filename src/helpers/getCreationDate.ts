import padZeros from './padZeros';

export default function getCreationDate() {
	const date = new Date();
	return `${padZeros(date.getDate(), 2)}/${padZeros(
		date.getMonth() + 1,
		2
	)}/${date.getFullYear()}`;
}
