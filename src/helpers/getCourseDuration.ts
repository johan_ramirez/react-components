import padZeros from './padZeros';

export default function getCourseDuration(duration: number) {
	const hours = Math.floor(duration / 60);
	const minutes = duration % 60;

	return `${padZeros(hours, 2)}:${padZeros(minutes, 2)} hour${
		hours > 1 ? 's' : ''
	}`;
}
